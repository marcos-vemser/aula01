

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * A classe de teste ElfoTest.
 *
 * @author  (seu nome)
 * @version (um número de versão ou data)
 */
public class ElfoTest
{
   @Test
   
   public void atirarFlechaDiminuirFlechaAumentarXPDiminuirVidaDwarf(){
       Elfo novoElfo = new Elfo("Legolas");
       Dwarf dwarf = new Dwarf("João");
        
       novoElfo.atirarFlecha(dwarf);
        
       
       assertEquals(1, novoElfo.getExp());
       assertEquals(3, novoElfo.getQntFlecha());
       assertEquals(100.0, 110.0, dwarf.getVida());
   }
}
